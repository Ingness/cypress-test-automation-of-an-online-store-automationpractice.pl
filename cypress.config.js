const { defineConfig } = require("cypress");

//  Multiple configuration files
const fs = require('fs-extra')
const path = require('path')
function getConfigurationByFile(file) {
  const pathToConfigFile = path.resolve('cypress', 'config', `${file}.env.json`)

  return fs.readJson(pathToConfigFile)
}

module.exports = defineConfig({
  e2e: {
    projectId: "vp6grn",
    baseUrl: "http://www.automationpractice.pl",
    chromeWebSecurity: true,
    video: true,

    setupNodeEvents(on, config) {
      console.log(config)
      //  Multiple configuration files
      const file = config.env.configFile || "origin"
      return getConfigurationByFile(file)
    },
  },
});
