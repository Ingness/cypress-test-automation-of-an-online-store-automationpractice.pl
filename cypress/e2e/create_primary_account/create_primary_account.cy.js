// Execute this only once, only if you want to create a new account with your own email address.
// Complete the cypress/config/custom.env.json file before executing.
describe("Create primary account", () => {
    beforeEach(() => {
      cy.visit("/")
      cy.get(".login").click()
    })
  
    it("Primary account creation", () => {
      cy.get("#email_create").type(Cypress.env("email"))
      cy.get("#SubmitCreate").click()
  
      cy.get("#id_gender2").check()
      cy.get("#customer_firstname").type(Cypress.env("name"))
      cy.get("#customer_lastname").type(Cypress.env("surname"))
      cy.get("#passwd").type(Cypress.env("password"))
      cy.get("#days").select("25")
      cy.get("#months").select("5")
      cy.get("#years").select("1996")
      cy.get("#submitAccount").click()
  
      cy.contains(".alert-success", "Your account has been created.")
    })
  
    it("Login to primary account", () => {
      cy.get("#email").type(Cypress.env("email"))
      cy.get("#passwd").type(Cypress.env("password"))
      cy.get("#SubmitLogin").click()
      cy.contains(".info-account", "Welcome to your account. Here you can manage all of your personal information and orders.")
    })
  })