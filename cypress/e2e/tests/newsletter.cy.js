describe("Contact us", () => {
    let email
    let email_alias
    beforeEach(() => {
      email_alias = Math.random().toString().substring(2,6)
      email = Cypress.env("email_username") + "+" + email_alias + "@" + Cypress.env("email_server&domain")
      //cy.wrap(email).as('email')

      cy.visit("/")
    })
  
    it("Correct email adress and press enter", () => {
      cy.get("#newsletter-input").type(email).type("{enter}")

      cy.contains(".alert-success", "Newsletter : You have successfully subscribed to this newsletter.")
    })

    it("Correct email adress and click the button", () => {
      cy.get("#newsletter-input").type(email)
      cy.get("[name='submitNewsletter']").click()
  
      cy.contains(".alert-success", "Newsletter : You have successfully subscribed to this newsletter.")
    })

    it("Incorrect email adress - email adress is already saved", () => {
      cy.get("#newsletter-input").type(email)
      cy.get("[name='submitNewsletter']").click()

      cy.get("#newsletter-input").type(email)
      cy.get("[name='submitNewsletter']").click()
  
      cy.contains(".alert-danger", "Newsletter : This email address is already registered.")
    })

    it("Incorrect email adress - invalid email adress", () => {
      cy.get("#newsletter-input").type("incorrect@mail")
      cy.get("[name='submitNewsletter']").click()
    
      cy.contains(".alert-danger", "Newsletter : Invalid email address.")
      })
})