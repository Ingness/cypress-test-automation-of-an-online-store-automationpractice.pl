describe("Contact us", () => {
    beforeEach(() => {
      cy.visit("/")
      cy.get("#contact-link").click()
    })
  
    it("Customer service without file and without order reference", () => {
      cy.get("#id_contact").select("2")
      cy.contains("#desc_contact2", "For any question about a product, an order")
      cy.get("#email").type(Cypress.env("email"))
      cy.get("#message").type("Test message")
      cy.get("#submitMessage").click()

      cy.contains(".alert-success", "Your message has been successfully sent to our team.")
    })

    it("Webmaster with file", () => {
      cy.get("#id_contact").select("1")
      cy.contains("#desc_contact1", "If a technical problem occurs on this website")
      cy.get("#email").type(Cypress.env("email"))
      cy.get('#fileUpload').attachFile("../fixtures/bug.jpg")
      cy.get(".filename").should("contain", "bug.jpg")
      cy.get("#message").type("Test message")
      cy.get("#submitMessage").click()

      cy.contains(".alert-success", "Your message has been successfully sent to our team.")
    })

    it("Customer service with order reference", () => {
      cy.get("#id_contact").select("2")
      cy.get("#email").type(Cypress.env("email"))
      cy.get("#id_order").type("123456")
      cy.get("#message").type("Test message")
      cy.get("#submitMessage").click()

      cy.contains(".alert-success", "Your message has been successfully sent to our team.")
    })

    it("Webmaster with file and order reference", () => {
      cy.get("#id_contact").select("1")
      cy.get("#email").type(Cypress.env("email"))
      cy.get("#id_order").type("123456")
      cy.get('#fileUpload').attachFile("../fixtures/bug.jpg")
      cy.get(".filename").should("contain", "bug.jpg")
      cy.get("#message").type("Test message")
      cy.get("#submitMessage").click()

      cy.contains(".alert-success", "Your message has been successfully sent to our team.")
    })

    it("No email adress", () => {
        cy.get("#id_contact").select("2")
        cy.get("#message").type("Test message")
        cy.get("#submitMessage").click()
        cy.contains(".alert-danger", "Invalid email address.")
    })

    it("Invalid email address", () => {
        cy.get("#id_contact").select("2")
        cy.get("#email").type("incorrect@mail")
        cy.get("#message").type("Test message")
        cy.get("#submitMessage").click()
        cy.contains(".alert-danger", "Invalid email address.")
    })

    it("No subject", () => {
        cy.get("#id_contact").select("0")
        cy.get("#email").type(Cypress.env("email"))
        cy.get("#message").type("Test message")
        cy.get("#submitMessage").click()
        cy.contains(".alert-danger", "Please select a subject from the list provided.")
      })

      it("No message", () => {
        cy.get("#id_contact").select("1")
        cy.get("#email").type(Cypress.env("email"))
        cy.get("#submitMessage").click()
        cy.contains(".alert-danger", "The message cannot be blank.")
      })

      it("Long message", () => {
        cy.get("#id_contact").select("1")
        cy.get("#email").type(Cypress.env("email"))
        cy.fixture('lorem_ipsum').then((text) => {
            cy.get("#message").type(text, {delay: 0})
        })
        cy.get("#message").type("Test message")
        cy.get("#submitMessage").click()

        cy.contains(".alert-success", "Your message has been successfully sent to our team.")
      })
  })