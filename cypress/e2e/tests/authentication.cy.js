describe("Create new account", () => {
  before(() => {
    // To create new accounts easily and quickly, aliases for email addresses were used.
    const email_alias = Math.random().toString().substring(2,6)
    const email = Cypress.env("email_username") + "+" + email_alias + "@" + Cypress.env("email_server&domain")
    cy.wrap(email).as('email')
  })

  beforeEach(() => {
    cy.visit("/")
    cy.get(".login").click()
  })

  it("Correct account creation", function() {
    cy.get("#email_create").type(this.email)
    cy.get("#SubmitCreate").click()

    cy.get("#id_gender2").check()
    cy.get("#customer_firstname").type(Cypress.env("name"))
    cy.get("#customer_lastname").type(Cypress.env("surname"))
    cy.get("#passwd").type(Cypress.env("password"))
    cy.get("#days").select("25")
    cy.get("#months").select("5")
    cy.get("#years").select("1996")
    cy.get("#submitAccount").click()

    cy.contains(".alert-success", "Your account has been created.")
  })

  it("Inorrect account creation - invalid email address", () => {
    cy.get("#email_create").type("incorrect@mail")
    cy.get("#SubmitCreate").click()
    cy.contains(".alert-danger", "Invalid email address.")
  })

  it("Inorrect account creation - email address already exist", function() {
    cy.get("#email_create").type(this.email)
    cy.get("#SubmitCreate").click()
    cy.contains(".alert-danger", "An account using this email address has already been registered. Please enter a valid password or request a new one.")
  })

  it("Correct login", function() {
    cy.get("#email").type(this.email)
    cy.get("#passwd").type(Cypress.env("password"))
    cy.get("#SubmitLogin").click()
    cy.contains(".info-account", "Welcome to your account. Here you can manage all of your personal information and orders.")
  })

  it("Incorrect login - invalid email address", () => {
    cy.get("#email").type("incorrect@mail")
    cy.get("#SubmitLogin").click()
    cy.contains(".alert-danger", "Invalid email address.")
  })

  it("Incorrect login - invalid login details", function() {
    cy.get("#email").type(this.email)
    cy.get("#passwd").type("wrong-password")
    cy.get("#SubmitLogin").click()
    cy.contains(".alert-danger", "Authentication failed.")
  })

  it("Logging out", function() {
    cy.get("#email").type(this.email)
    cy.get("#passwd").type(Cypress.env("password"))
    cy.get("#SubmitLogin").click()
    cy.get(".logout").click()
    cy.url().should("eq", "http://www.automationpractice.pl/index.php?controller=authentication&back=my-account")
  })

  it("Correct forgotting password", function() {
    cy.get("[title='Recover your forgotten password']").click()
    cy.get("#email").type(this.email)
    cy.contains("button", "Retrieve Password").click()
    const text = "A confirmation email has been sent to your address: " + this.email
    cy.contains(".alert-success", text)
    // E-mail will not arrive, there is a problem with the website.
  })

  it("Inorrect forgotting password - invalid email adress", function() {
    cy.get("[title='Recover your forgotten password']").click()
    cy.get("#email").type("incorrect@mail")
    cy.contains("button", "Retrieve Password").click()
    cy.contains(".alert-danger", "Invalid email address.")
  })

  it("Inorrect forgotting password - email doesn't exist", function() {
    cy.get("[title='Recover your forgotten password']").click()
    cy.get("#email").type("correct@email.com")
    cy.contains("button", "Retrieve Password").click()
    cy.contains(".alert-danger", "There is no account registered for this email address.")
  })
})