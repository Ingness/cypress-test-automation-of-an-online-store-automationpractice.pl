npm install --save-dev cypress-file-upload

import 'cypress-file-upload' -> cypress/support/commands.js

Cypress Cloud
1. Follow the project setup instruction from Cypress Cloud (I used GitLab CI)
2. Remember to change projectID in cypress.config.js and CYPRESS_RECORD_KEY in .gitlab-ci.yml (if you use GitLab too)
3. If you want to use your data from custom.env.json - change "run-all" to "run_all_custom" in .gitlab-ci.yml (17 row)