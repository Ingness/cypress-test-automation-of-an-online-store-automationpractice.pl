# Cypress test automation of an online store automationpractice.pl


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Installation
Paste the following instructions into the project's terminal

```
cd existing_repo
git clone https://gitlab.com/Ingness/cypress-test-automation-of-an-online-store-automationpractice.pl.git

npm install --save-dev cypress-file-upload
```

Then add to your cypress/support/commands.js file
```
import 'cypress-file-upload'
```


## Cypress Cloud
If you want to check test results in Cypress Cloud:
1. Create new project on your account in Cypress Cloud and follow the project setup instruction from Cypress Cloud (I used GitLab CI). More information about Cypress Cloud: https://docs.cypress.io/guides/cloud/introduction
2. Remember to change projectID in cypress.config.js and CYPRESS_RECORD_KEY in .gitlab-ci.yml (if you use GitLab too).
3. If you want to use your data from custom.env.json - change

```
    - npm run run-all -- --record
```
to 
```
    - npm run run-all-custom -- --record
```
in .gitlab-ci.yml (17 row).